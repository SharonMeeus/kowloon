kowloonApp.directive("kowloonHeader", function () {
	return {
		restrict: "E",
		templateUrl: "app/directives/header/header.html",
		replace: true,
		scope: {},
		bindToController: true,
		controllerAs: "header",
		controller: function ($state,$cookies) {
			
			var vm = this;
			vm.selected = 0;	
			vm.iconhover = 0;
			vm.src = "";

			vm.nav_items = [
			{ name: "", id: "hamburger-icon", alt:"menu icon", src: "" },
			{ name: "Search", id: "search-icon", alt:"search icon", src: "search" },
			{ name: "FAQ", id: "faq-icon", alt:"faq icon", src: "faq" },
			{ name: "About Us", id: "mail-icon", alt:"mail icon", src: "about" },
			{ name: "Dogs", id: "dogs-icon", alt:"dogs icon", src: ["productview", {category: 'dogs'}] },
			{ name: "Cats", id: "cats-icon", alt:"cats icon", src: ["productview", {category: 'cats'}] },
			{ name: "Fish", id: "fish-icon", alt:"fish icon", src: ["productview", {category: 'fish'}] },
			{ name: "Birds", id: "birds-icon", alt:"birds icon", src: ["productview", {category: 'birds'}] },
			{ name: "Small Animals", id: "sa-icon", alt:"small animals icon", src: ["productview", {category: 'small-animals'}] },
			];

			vm.navEnter = function () {
				$("#nav span").show();
				$("#nav .logo").css({background: "url('../assets/img/Kowloon-Icons-50.png') 200px 0", width: "200px"});
				$("#nav").stop().animate({width:"210px"}, 400);
			}

			vm.navLeave = function () {
				$("#nav").stop().animate({width:"50px"}, 400, function() { 
					$("#nav span").hide();
					$("#nav .logo").css({background: "url('../assets/img/Kowloon-Icons-50.png') -50px 0", width: "50px"}); 
				});
			}

			vm.checkSrc = function (src) {
				if(src === 'search') {
					$('#faq').hide();
					$('#search').addClass('active');
				} else if(src === 'faq') {
					$('#search').hide();
					$('#faq').addClass('active');
				} else { 
					$('#search, #faq').removeClass('active');
					if(Array.isArray(src)) {
						$state.go(src[0], src[1]);
					} else { $state.go(src); }
					
				}
			}

			function checkCookie() {
				if($cookies.get('Kowloon-cookie') == null) {
					console.log("has no cookie");
					swal({
						title: "Cookies",
						text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna Duis voluptate velit esse cillum dolore eu fugiat nulla pariatu",
						confirmButtonText: "ok, verder surfen",
						allowOutsideClick: false
					}).then(function () {
						let rnd = (Math.random()*1e32).toString(36);
						var expire = new Date();
						expire.setDate(expire.getDate() + 30);
						$cookies.put("Kowloon-cookie", "kowloon-" + rnd, expire);
					})
				} else {
					console.log("has cookie");
				}
			}

			checkCookie();

		}
	}
});