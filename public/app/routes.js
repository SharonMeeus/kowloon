kowloonApp.config(function ($stateProvider, $urlRouterProvider) {

 $urlRouterProvider.otherwise("/");

  $stateProvider
	.state("home", {
	  url: "/",
	  templateUrl: "app/pages/home.html",
	  controller: "homeController as home"
	})
	.state("about", {
	  url: "/about",
	  templateUrl: "app/pages/about.html",
	  controller: "aboutController as about"
	})
	.state("productview", {
	  url: "/view/products/{category}",
	  templateUrl: "app/pages/product_view.html",
	  controller: "productViewController as productview"
	})
	.state("productdetails", {
	  url: "/view/{category}/item/productdetails",
	  templateUrl: "app/pages/product_details.html",
	  controller: "productDetailsController as productdetails"
	});
});