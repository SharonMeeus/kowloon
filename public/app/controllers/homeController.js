kowloonApp.controller("homeController", function () {

	vm = this;

	vm.slides = ["header-img1.jpg", "header-img2.jpg", "header-img3.jpg"];
	vm.icons = [{name: "dogs", id: "dogs"}, {name: "cats", id: "cats"}, 
				{name: "fish", id: "fish"}, {name: "birds", id: "birds"}, 
				{name: "small animals", id: "sa"}, {name: "other", id: "plus"}];

	vm.hover = "id";

	//title, price, img, colours, 
	vm.hot_items = [
		{title: "Cooling mat", price: "15.49", img: "hot-item-details.png", colours:["white", "black", "blue"], category: "dogs"},
		{title: "Cooling mat", price: "16.49", img: "hot-item-details.png", colours:["white", "black", "blue"], category: "cats"},
		{title: "Cooling mat", price: "17.49", img: "hot-item-details.png", colours:[], category: "birds"},
	]

	vm.startSlider = function () {
		$('.slider').slider();
	}

});