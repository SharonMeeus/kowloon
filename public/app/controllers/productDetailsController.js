kowloonApp.controller("productDetailsController", function ($stateParams) {

	var maxPrice = 2000;
	var slider;	

	vm = this;
	vm.currentCategory = $stateParams.category;
	vm.activePicture = 1;
	vm.pictureCollection = [{id: 1, img: "about-header.jpg"}, {id: 2, img: "hot-item.jpg"}, {id: 3, img: "hot-item.jpg"}];

	vm.currentItem = {title: "Cooling mat", price: 57, img: "hot-item.jpg", colours: ["white", "black", "blue"], info: "Hier komt een de volledige beschrijvende tekst met een max. aantal karakters. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea clit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consectetur.", category: "Splash 'n Fun"}
	vm.relatedItems = new Array(16);
	vm.questions = getDummyData();

	
	$(document).ready(() => {
		$('.collapsible').collapsible();

		$('#related-imgs').bxSlider({
			slideWidth: 900,
			minSlides: 4,
			maxSlides: 5,
			slideMargin: 10
		});
	});

	vm.toggleCaret = function(el) {
		
		let element = $(el.currentTarget.firstElementChild.firstElementChild);
		if($(element).hasClass('fa-caret-right')) {
			$(element).removeClass('fa-caret-right').addClass('fa-caret-down');
		} else {
			$(element).removeClass('fa-caret-down').addClass('fa-caret-right');
		}

	}


	function getDummyData() {
		let _arr = [];

		for(let i = 0; i < 4; i++) {
			let obj = {title: "Dit is een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."}
			_arr.push(obj);
		}

		return _arr;
	}

});