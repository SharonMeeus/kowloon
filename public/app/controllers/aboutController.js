kowloonApp.controller("aboutController", function () {

	vm = this;

	vm.active_question = -1;
	vm.questions = [
		{title: "Dit is een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
		{title: "Dit is nog een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
		{title: "En weer een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
	];

	vm.collapse = function () {
		$('.collapsible').collapsible();
	}

	vm.toggleCaret = function(el) {
		
		let element = $(el.currentTarget.firstElementChild.firstElementChild);
		if($(element).hasClass('fa-caret-right')) {
			$(element).removeClass('fa-caret-right').addClass('fa-caret-down');
		} else {
			$(element).removeClass('fa-caret-down').addClass('fa-caret-right');
		}

	}

});