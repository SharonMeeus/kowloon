kowloonApp.controller("productViewController", function ($stateParams, $scope) {

	var maxPrice = 2000;
	var slider;	

	vm = this;
	vm.showFilter = true;
	vm.selectedSort = "relevance";
	vm.currentCategory = $stateParams.category;
	vm.numberToDisplay = 8;

	$scope.minrange = 0;
	$scope.maxrange = 2000;


	// Dummy data
	vm.ddSortNames = ["relevance", "price: low to high", "price: high to low", "latest", "oldest"];
	vm.slides = ["header-img1.jpg", "header-img2.jpg", "header-img3.jpg"];
	vm.nameFilters = [{name: "Splash 'n Fun"}, {name: "Luxury"}, {name: "new"},{name: "on sale"}, {name: "other"}];
	vm.all_items = getDummyItems();

	vm.filterCategories = function () {
		let selected_categories = getCheckboxCategories();
		return function (item) {
			if(selected_categories.length > 0) {
				for(let x = 0; x < selected_categories.length; x++) {
					if(item.category.name.toLowerCase() === selected_categories[x].toLowerCase()) {
						return true;
					}
				}
			} else {return true;}
		}
	}

	vm.updateSliders = function (slidername) {
		if (slidername === 'min') {
			if($scope.minrange > 2000 || $scope.maxrange <= $scope.minrange) {
				$scope.minrange = 0;
			}
			slider.noUiSlider.set([$scope.minrange, null]);
		} else if(slidername === 'max') {
			if($scope.maxrange > 2000 || $scope.maxrange <= $scope.minrange) {
				$scope.maxrange = 2000;
			}
			slider.noUiSlider.set([null, $scope.maxrange]);
		}
	}

	vm.filterPrice = function () {
		return function (item) {
			if(item.price > $scope.minrange && item.price < $scope.maxrange) 
				return true;
		}
	}

	vm.loadMore = function() {
		if (vm.numberToDisplay + 4 < vm.all_items.length) {
			vm.numberToDisplay += 4	;
		} else {
			vm.numberToDisplay = vm.all_items.length;
		}
	};

	function getDummyItems() {
		let _arr = [];
		for (let i=0; i < 50; i++) {
			let rnd_price = Math.floor((Math.random() * 2000) + 1);
			let colours = [];
			if(rnd_price % 2 === 0) colours = ["white", "black", "blue"];

			let obj = {title: "Cooling mat " + (i + 1), price: rnd_price, img: "hot-item.jpg", colours: colours, info: "Info about this product Lorem ipsum blablablablabala", category: vm.nameFilters[Math.floor((Math.random() * 5))]}

			_arr.push(obj);
		}

		return _arr;
	}

	function getCheckboxCategories() {
		let selected_categories = [];

		angular.forEach(vm.nameFilters,(category) => {
			if(category.selected) {
				selected_categories.push(category.name);
			}
		});
		return selected_categories;
	}
	

	function _init() {
		
		slider = document.getElementById("range-slider");
		noUiSlider.create(slider, {
			start: [0, 2000],
			connect: true,
			step: 1,
   			orientation: 'horizontal', // 'horizontal' or 'vertical'
   			range: {
   				'min': 0,
   				'max': maxPrice
   			}
   		});

	}


	$(document).ready(() => {
		$('.slider').slider();
		$('.dropdown-button').dropdown(
			{belowOrigin: true}
			);
		_init();
		slider.noUiSlider.on('slide', function( value ){
			$scope.$apply(function () {
				$scope.minrange = parseInt(value[0]);
				$scope.maxrange = parseInt(value[1]);
			})
			
		});
	})

	

});