var kowloonApp = angular.module('kowloonApp', ['ui.router', 'ngCookies', 'infinite-scroll']);
kowloonApp.config(function ($stateProvider, $urlRouterProvider) {

 $urlRouterProvider.otherwise("/");

  $stateProvider
	.state("home", {
	  url: "/",
	  templateUrl: "app/pages/home.html",
	  controller: "homeController as home"
	})
	.state("about", {
	  url: "/about",
	  templateUrl: "app/pages/about.html",
	  controller: "aboutController as about"
	})
	.state("productview", {
	  url: "/view/products/{category}",
	  templateUrl: "app/pages/product_view.html",
	  controller: "productViewController as productview"
	})
	.state("productdetails", {
	  url: "/view/{category}/item/productdetails",
	  templateUrl: "app/pages/product_details.html",
	  controller: "productDetailsController as productdetails"
	});
});
kowloonApp.controller("aboutController", function () {

	vm = this;

	vm.active_question = -1;
	vm.questions = [
		{title: "Dit is een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
		{title: "Dit is nog een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
		{title: "En weer een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."},
	];

	vm.collapse = function () {
		$('.collapsible').collapsible();
	}

	vm.toggleCaret = function(el) {
		
		let element = $(el.currentTarget.firstElementChild.firstElementChild);
		if($(element).hasClass('fa-caret-right')) {
			$(element).removeClass('fa-caret-right').addClass('fa-caret-down');
		} else {
			$(element).removeClass('fa-caret-down').addClass('fa-caret-right');
		}

	}

});
kowloonApp.controller("homeController", function () {

	vm = this;

	vm.slides = ["header-img1.jpg", "header-img2.jpg", "header-img3.jpg"];
	vm.icons = [{name: "dogs", id: "dogs"}, {name: "cats", id: "cats"}, 
				{name: "fish", id: "fish"}, {name: "birds", id: "birds"}, 
				{name: "small animals", id: "sa"}, {name: "other", id: "plus"}];

	vm.hover = "id";

	//title, price, img, colours, 
	vm.hot_items = [
		{title: "Cooling mat", price: "15.49", img: "hot-item-details.png", colours:["white", "black", "blue"], category: "dogs"},
		{title: "Cooling mat", price: "16.49", img: "hot-item-details.png", colours:["white", "black", "blue"], category: "cats"},
		{title: "Cooling mat", price: "17.49", img: "hot-item-details.png", colours:[], category: "birds"},
	]

	vm.startSlider = function () {
		$('.slider').slider();
	}

});
kowloonApp.controller("productDetailsController", function ($stateParams) {

	var maxPrice = 2000;
	var slider;	

	vm = this;
	vm.currentCategory = $stateParams.category;
	vm.activePicture = 1;
	vm.pictureCollection = [{id: 1, img: "about-header.jpg"}, {id: 2, img: "hot-item.jpg"}, {id: 3, img: "hot-item.jpg"}];

	vm.currentItem = {title: "Cooling mat", price: 57, img: "hot-item.jpg", colours: ["white", "black", "blue"], info: "Hier komt een de volledige beschrijvende tekst met een max. aantal karakters. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea clit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consectetur.", category: "Splash 'n Fun"}
	vm.relatedItems = new Array(16);
	vm.questions = getDummyData();

	
	$(document).ready(() => {
		$('.collapsible').collapsible();

		$('#related-imgs').bxSlider({
			slideWidth: 900,
			minSlides: 4,
			maxSlides: 5,
			slideMargin: 10
		});
	});

	vm.toggleCaret = function(el) {
		
		let element = $(el.currentTarget.firstElementChild.firstElementChild);
		if($(element).hasClass('fa-caret-right')) {
			$(element).removeClass('fa-caret-right').addClass('fa-caret-down');
		} else {
			$(element).removeClass('fa-caret-down').addClass('fa-caret-right');
		}

	}


	function getDummyData() {
		let _arr = [];

		for(let i = 0; i < 4; i++) {
			let obj = {title: "Dit is een vraag", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit.."}
			_arr.push(obj);
		}

		return _arr;
	}

});
kowloonApp.controller("productViewController", function ($stateParams, $scope) {

	var maxPrice = 2000;
	var slider;	

	vm = this;
	vm.showFilter = true;
	vm.selectedSort = "relevance";
	vm.currentCategory = $stateParams.category;
	vm.numberToDisplay = 8;

	$scope.minrange = 0;
	$scope.maxrange = 2000;


	// Dummy data
	vm.ddSortNames = ["relevance", "price: low to high", "price: high to low", "latest", "oldest"];
	vm.slides = ["header-img1.jpg", "header-img2.jpg", "header-img3.jpg"];
	vm.nameFilters = [{name: "Splash 'n Fun"}, {name: "Luxury"}, {name: "new"},{name: "on sale"}, {name: "other"}];
	vm.all_items = getDummyItems();

	vm.filterCategories = function () {
		let selected_categories = getCheckboxCategories();
		return function (item) {
			if(selected_categories.length > 0) {
				for(let x = 0; x < selected_categories.length; x++) {
					if(item.category.name.toLowerCase() === selected_categories[x].toLowerCase()) {
						return true;
					}
				}
			} else {return true;}
		}
	}

	vm.updateSliders = function (slidername) {
		if (slidername === 'min') {
			if($scope.minrange > 2000 || $scope.maxrange <= $scope.minrange) {
				$scope.minrange = 0;
			}
			slider.noUiSlider.set([$scope.minrange, null]);
		} else if(slidername === 'max') {
			if($scope.maxrange > 2000 || $scope.maxrange <= $scope.minrange) {
				$scope.maxrange = 2000;
			}
			slider.noUiSlider.set([null, $scope.maxrange]);
		}
	}

	vm.filterPrice = function () {
		return function (item) {
			if(item.price > $scope.minrange && item.price < $scope.maxrange) 
				return true;
		}
	}

	vm.loadMore = function() {
		if (vm.numberToDisplay + 4 < vm.all_items.length) {
			vm.numberToDisplay += 4	;
		} else {
			vm.numberToDisplay = vm.all_items.length;
		}
	};

	function getDummyItems() {
		let _arr = [];
		for (let i=0; i < 50; i++) {
			let rnd_price = Math.floor((Math.random() * 2000) + 1);
			let colours = [];
			if(rnd_price % 2 === 0) colours = ["white", "black", "blue"];

			let obj = {title: "Cooling mat " + (i + 1), price: rnd_price, img: "hot-item.jpg", colours: colours, info: "Info about this product Lorem ipsum blablablablabala", category: vm.nameFilters[Math.floor((Math.random() * 5))]}

			_arr.push(obj);
		}

		return _arr;
	}

	function getCheckboxCategories() {
		let selected_categories = [];

		angular.forEach(vm.nameFilters,(category) => {
			if(category.selected) {
				selected_categories.push(category.name);
			}
		});
		return selected_categories;
	}
	

	function _init() {
		
		slider = document.getElementById("range-slider");
		noUiSlider.create(slider, {
			start: [0, 2000],
			connect: true,
			step: 1,
   			orientation: 'horizontal', // 'horizontal' or 'vertical'
   			range: {
   				'min': 0,
   				'max': maxPrice
   			}
   		});

	}


	$(document).ready(() => {
		$('.slider').slider();
		$('.dropdown-button').dropdown(
			{belowOrigin: true}
			);
		_init();
		slider.noUiSlider.on('slide', function( value ){
			$scope.$apply(function () {
				$scope.minrange = parseInt(value[0]);
				$scope.maxrange = parseInt(value[1]);
			})
			
		});
	})

	

});
kowloonApp.directive("kowloonFaq", function () {
	return {
		restrict: "E",
		templateUrl: "app/directives/faq/faq.html",
		replace: true,
		scope: {},
		bindToController: true,
		controllerAs: "faq",
		controller: function ($cookies) {
			
			var vm = this;
			

		}
	}
});
kowloonApp.directive("kowloonHeader", function () {
	return {
		restrict: "E",
		templateUrl: "app/directives/header/header.html",
		replace: true,
		scope: {},
		bindToController: true,
		controllerAs: "header",
		controller: function ($state,$cookies) {
			
			var vm = this;
			vm.selected = 0;	
			vm.iconhover = 0;
			vm.src = "";

			vm.nav_items = [
			{ name: "", id: "hamburger-icon", alt:"menu icon", src: "" },
			{ name: "Search", id: "search-icon", alt:"search icon", src: "search" },
			{ name: "FAQ", id: "faq-icon", alt:"faq icon", src: "faq" },
			{ name: "About Us", id: "mail-icon", alt:"mail icon", src: "about" },
			{ name: "Dogs", id: "dogs-icon", alt:"dogs icon", src: ["productview", {category: 'dogs'}] },
			{ name: "Cats", id: "cats-icon", alt:"cats icon", src: ["productview", {category: 'cats'}] },
			{ name: "Fish", id: "fish-icon", alt:"fish icon", src: ["productview", {category: 'fish'}] },
			{ name: "Birds", id: "birds-icon", alt:"birds icon", src: ["productview", {category: 'birds'}] },
			{ name: "Small Animals", id: "sa-icon", alt:"small animals icon", src: ["productview", {category: 'small-animals'}] },
			];

			vm.navEnter = function () {
				$("#nav span").show();
				$("#nav .logo").css({background: "url('../assets/img/Kowloon-Icons-50.png') 200px 0", width: "200px"});
				$("#nav").stop().animate({width:"210px"}, 400);
			}

			vm.navLeave = function () {
				$("#nav").stop().animate({width:"50px"}, 400, function() { 
					$("#nav span").hide();
					$("#nav .logo").css({background: "url('../assets/img/Kowloon-Icons-50.png') -50px 0", width: "50px"}); 
				});
			}

			vm.checkSrc = function (src) {
				if(src === 'search') {
					$('#faq').hide();
					$('#search').addClass('active');
				} else if(src === 'faq') {
					$('#search').hide();
					$('#faq').addClass('active');
				} else { 
					$('#search, #faq').removeClass('active');
					if(Array.isArray(src)) {
						$state.go(src[0], src[1]);
					} else { $state.go(src); }
					
				}
			}

			function checkCookie() {
				if($cookies.get('Kowloon-cookie') == null) {
					console.log("has no cookie");
					swal({
						title: "Cookies",
						text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna Duis voluptate velit esse cillum dolore eu fugiat nulla pariatu",
						confirmButtonText: "ok, verder surfen",
						allowOutsideClick: false
					}).then(function () {
						let rnd = (Math.random()*1e32).toString(36);
						var expire = new Date();
						expire.setDate(expire.getDate() + 30);
						$cookies.put("Kowloon-cookie", "kowloon-" + rnd, expire);
					})
				} else {
					console.log("has cookie");
				}
			}

			checkCookie();

		}
	}
});
kowloonApp.directive("kowloonSearch", function () {
	return {
		restrict: "E",
		templateUrl: "app/directives/search/search.html",
		replace: true,
		scope: {},
		bindToController: true,
		controllerAs: "faq",
		controller: function ($cookies) {
			
			var vm = this;
			

		}
	}
});
