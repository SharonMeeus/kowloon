<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kowloon</title>

	<link rel="stylesheet" href="node_modules/materialize-css/dist/css/materialize.min.css">
    <link rel="stylesheet" href="node_modules/nouislider/distribute/nouislider.min.css">
    <link rel="stylesheet" type="text/css" href="node_modules/bxslider/dist/jquery.bxslider.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
	<link rel="stylesheet" href="app/dist/css/app.scss">
</head>
<body ng-app="kowloonApp">
	<kowloon-header></kowloon-header>
    <kowloon-search></kowloon-search>
    <kowloon-faq></kowloon-faq>
	<div ui-view></div>
<!-- JS -->
<!-- CDN scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-cookies.min.js"></script>
    <script src="https://use.fontawesome.com/24f215a663.js" async ></script>

    <!-- Application scripts -->
    <script type="text/javascript" src="node_modules/materialize-css/dist/js/materialize.min.js"></script>
    <script type="text/javascript" src="node_modules/nouislider/distribute/nouislider.min.js"></script>
    <script type="text/javascript" src="node_modules/ng-infinite-scroll/build/ng-infinite-scroll.min.js"></script>
    <script type="text/javascript"src="node_modules/bxslider/dist/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="app/dist/js/scripts.js"></script>
</body>
</html>